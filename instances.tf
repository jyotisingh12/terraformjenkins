resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.example.id}"
  instance_id = "${aws_instance.webserver-1.id}"
}

resource "aws_instance" "webserver-1" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    subnet_id = "${aws_subnet.public-sn-1.id}"
    associate_public_ip_address = true
    availability_zone = "${var.az1}"
    vpc_security_group_ids = ["${aws_security_group.webservers_sg.id}"]
    user_data = "${file("./scripts/server1.sh")}"
    tags {
      Name = "terraform-webserver-1"
    }
}

resource "aws_instance" "webserver-2" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    subnet_id = "${aws_subnet.public-sn-2.id}"
    associate_public_ip_address = true
    availability_zone = "${var.az2}"
    vpc_security_group_ids = ["${aws_security_group.webservers_sg.id}"]
    user_data = "${file("./scripts/server2.sh")}"
    tags {
      Name = "terraform-webserver-2"
    }
}


resource "aws_ebs_volume" "example" {
  availability_zone = "${var.az1}"
  size              = 8
}
