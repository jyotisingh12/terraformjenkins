#! /bin/bash
yum update -y
yum install -y httpd java-1.8.0
wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
yum install jenkins -y
service jenkins start
chkconfig jenkins on
systemctl start jenkins.service
systemctl enable jenkins.service
service httpd start
chkconfig httpd on
echo "<!DOCTYPE html>" > /var/www/html/index.html
echo "<html lang="en-us">" >> /var/www/html/index.html
echo "<head>" >> /var/www/html/index.html
echo "  <title>1</title>" >> /var/www/html/index.html
echo "</head>" >> /var/www/html/index.html
echo "<body>" >> /var/www/html/index.html
echo "" >> /var/www/html/index.html
echo "<h1>It's Working!!!</h1>" >> /var/www/html/index.html
echo "<h2>Response from server 1</h2>" >> /var/www/html/index.html
echo "<h2>Deployed By Terraform</h2>" >> /var/www/html/index.html
echo "" >> /var/www/html/index.html
echo "</body>" >> /var/www/html/index.html
echo "</html>" >> /var/www/html/index.html
echo "" >> /var/www/html/index.html
