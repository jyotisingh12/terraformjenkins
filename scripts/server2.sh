#!/bin/bash
mkfs -t ext4 /dev/xvdg
mkdir /data
mount /dev/xvdg /data
echo /dev/xvdg  /data ext4 defaults,nofail 0 2 >> /etc/fstab

yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
echo "<!DOCTYPE html>" > /var/www/html/index.html
echo "<html lang="en-us">" >> /var/www/html/index.html
echo "<head>" >> /var/www/html/index.html
echo "<title>2</title>" >> /var/www/html/index.html
echo "</head>" >> /var/www/html/index.html
echo "<body>" >> /var/www/html/index.html
echo "" >> /var/www/html/index.html
echo "<h1>It's Working!!!</h1>" >> /var/www/html/index.html
echo "<h2>Response from server 2</h2>" >> /var/www/html/index.html
echo "<h2>Deployed By Terraform</h2>" >> /var/www/html/index.html
echo "</body>" >> /var/www/html/index.html
echo "</html>" >> /var/www/html/index.html
echo "" >> /var/www/html/index.html
