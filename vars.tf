variable "region" {
  default = "us-east-1"
}

variable "az1" {
  default = "us-east-1a"
}
variable "az2" {
  default = "us-east-1b"
}
variable "test_app_vpc_cidr" {
  default = "192.168.0.0/16"
}

variable "public_sn_1_cidr" {
  default = "192.168.1.0/24"
}

variable "public_sn_2_cidr" {
  default = "192.168.2.0/24"
}

variable "ami" {
  default = "ami-0323c3dd2da7fb37d"
}

variable "instance_type" {
  default = "t2.micro"
}
